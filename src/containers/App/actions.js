// import State chung từ Constants

import { LOAD_PAGE, LOAD_PAGE_SUCCESS, LOAD_PAGE_ERROR } from './constant';

// Gửi các export function khi Saga yêu cầu:

export function loadPage() {
  return {
    type: LOAD_PAGE,
  };
}

export function loadPageSuccess() {
  return {
    type: LOAD_PAGE_SUCCESS,
  };
}

export function loadPageError(error) {
  return {
    type: LOAD_PAGE_ERROR,
    error,
  };
}
