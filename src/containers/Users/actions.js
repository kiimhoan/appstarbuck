// import những action chứa trong constants

import { FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS, FETCH_USERS_FAILURE } from './constants';

export const fetchUsersRequest = () => ({
  type: FETCH_USERS_REQUEST,
});

export const fetchUsersSuccess = data => ({
  type: FETCH_USERS_SUCCESS,
  data,
});

export const fetchUsersFailure = error => ({
  type: FETCH_USERS_FAILURE,
  error,
});
