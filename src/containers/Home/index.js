import React from 'react';
import Navigation from '../../components/Navigation';
import './styles.scss';

const Home = () => (
  <div className="home-wrapper">
    <div className="menu-wrap">
      <Navigation />
      <div className="register-wrap">
        <div className="home-text">
          <h1>Good morning.</h1>
          <img alt="" src="https://via.placeholder.com/48x48" />
        </div>
        <div className="connect-wrap">
          <div className="sign-in">
            <img className="logo-signin" src="https://via.placeholder.com/20x20" alt="signin" />
            <a href="/sign-in">Sign in</a>
          </div>
          <div className="join-now">
            <button className="jn-btn" type="button">
              Join now
            </button>
          </div>
        </div>
      </div>
    </div>

    <div className="content-wrap">
      <div className="intro-starbucks">
        <div className="intro-text">
          <h1>STARBUCKS® REWARDS</h1>
        </div>
        <div className="intro-img">
          <div className="intro1">
            <img alt="" src="https://via.placeholder.com/200x100" />
            <p>Let Starbucks® Rewards add a little joy to your day</p>
          </div>
          <div className="intro1">
            <img alt="" src="https://via.placeholder.com/200x100" />
            <p>Signing up is easy and fast</p>
          </div>
          <div className="intro1">
            <img alt="" src="https://via.placeholder.com/200x100" />
            <p>Exclusive offers, personalized for you</p>
          </div>
        </div>
        <div className="more-btn">
          <button className="jn-btn" type="button">
            Join now
          </button>
          <button className="lm-btn" type="button">
            Learn more
          </button>
        </div>
      </div>

      <div className="banner-home">
        <div className="banner-logo">
          <img alt="" src="https://via.placeholder.com/500x400" />
          <div className="banner-text">
            <h2> You are up to date</h2>
            <img alt="" src="https://via.placeholder.com/20x20" />
          </div>
        </div>
        <div className="line" />
        <div className="terms-policy">
          <div className="change-region">
            <p>US</p>
            <button className="cr-btn" type="button">
              Change region
            </button>
          </div>
          <div className="terms">
            <a href="./respon">Responsibility</a>
            <a href="./web">Web Accessibility</a>
            <a href="./pri">Privacy Policy</a>
            <a href="./term">Terms of Use</a>
          </div>
        </div>
        <div className="ds-by">
          <p>© 2019 Starbucks</p>
        </div>
      </div>
      <div className="orders-btn">
        <button className="ord-btn" type="button">
          Start an order
        </button>
      </div>
    </div>
  </div>
);

export default Home;
