import { createSelector } from 'reselect';
import { initialState } from './reducer';

// initstate hien g la object user trong reducers global
const selectUser = state => state.user || initialState;

// selector lam viec voi reducers global
// đóng vai trò cập nhật những state -> đẩy vào vùng nhớ mới,ko có state thì mặc định trạng thái initstate
const makeSelectUsers = () =>
  createSelector(
    selectUser,
    userState => userState.users,
  );

const makeSelectIsFetchingUsers = () =>
  createSelector(
    selectUser,
    userState => userState.isFetchingUsers,
  );

const makeSelectIsFetchingUsersError = () =>
  createSelector(
    selectUser,
    userState => userState.isFetchingUsersError,
  );

export { selectUser, makeSelectUsers, makeSelectIsFetchingUsers, makeSelectIsFetchingUsersError };
