import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { makeSelectUsers, makeSelectIsFetchingUsers, makeSelectIsFetchingUsersError } from './selectors';
import { fetchUsersRequest } from './actions';
import Navigation from '../../components/Navigation';
import { TitleWrapper } from './styled';
import './styles.scss';

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { getUsers } = this.props;
    // thực hiện gọi api = getUsers
    getUsers();
  }

  render() {
    // tạo props bằng cách import những state từ selector qua
    const { isFetchingUsers, users } = this.props;
    return (
      <div className="home-wrapper">
        <div className="menu-wrap">
          <Navigation />
          <div className="users-wrap">
            <p>PAGE USER</p>
          </div>
        </div>
        <div className="content-wrap">
          <TitleWrapper>LIST USERS</TitleWrapper>
          {isFetchingUsers && 'Loading...'}
          {users.map(user => (
            <div className="users-card">
              <div className="users-avatar">
                <img className="img-1" src={user.avatar_url} alt="avatar" />
              </div>
              <div className="users-infor">
                <p>ID: {user.id}</p>
                <p>NAME: {user.login}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
// Checktype trong màn hình Users gồm có những props nào
Users.propTypes = {
  getUsers: PropTypes.func,
  users: PropTypes.array,
  isFetchingUsers: PropTypes.bool,
  isFetchingUsersError: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  users: makeSelectUsers(),
  isFetchingUsers: makeSelectIsFetchingUsers(),
  isFetchingUsersError: makeSelectIsFetchingUsersError(),
});

// goi api -> goi actions can xu li
const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(fetchUsersRequest()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Users);
