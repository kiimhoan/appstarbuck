import React from 'react';
// import Button from 'antd/lib/button';
// import Linhtinh from '../../components/Button/button';
import Navigation from '../../components/Navigation';
// import Linhtinh from '../../components/Button/button';
// import Choose from '../../components/Choose';
import './styles.scss';

const Order = () => (
  <div className="home-wrapper">
    <div className="menu-wrap">
      <Navigation />
      <div className="orders-wrap">
        <p>HELLo</p>
      </div>
    </div>

    <div className="content-wrap">
      <div className="drinks-wrapper">
        <p className="text">DRINKS</p>
        <div className="all-drinks">
          <div className="drinks-left">
            <div className="drinks-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Hot Coffees</p>
            </div>
            <div className="drinks-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Hot Drinks</p>
            </div>
            <div className="drinks-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Cold Coffees</p>
            </div>
            <div className="drinks-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Cold Drinks</p>
            </div>
          </div>
          <div className="drinks-right">
            <div className="drinks-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Hot Teas</p>
            </div>
            <div className="drinks-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Frappuccino Blended Beverages</p>
            </div>
            <div className="drinks-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Iced Teas</p>
            </div>
          </div>
        </div>
      </div>
      <div className="foods-wrapper">
        <p className="text">FOOD</p>
        <div className="all-foods">
          <div className="foods-left">
            <div className="foods-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Hot breakfast</p>
            </div>
            <div className="foods-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Lunch</p>
            </div>
            <div className="foods-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Yogurt & Custard</p>
            </div>
          </div>
          <div className="foods-right">
            <div className="foods-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Bakery</p>
            </div>
            <div className="foods-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Snacks & Sweets</p>
            </div>
          </div>
        </div>
      </div>
      <div className="coffees-wrapper">
        <p className="text">AT HOME COFFEE</p>
        <div className="all-coffees">
          <div className="coffees-left">
            <div className="coffees-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Whole Bean</p>
            </div>
            <div className="coffees-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>VIA Instant</p>
            </div>
          </div>
          <div className="coffees-right">
            <div className="coffees-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Verismo Pods</p>
            </div>
            <div className="coffees-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Cold Brew</p>
            </div>
          </div>
        </div>
      </div>
      <div className="bags-wrapper">
        <p className="text">SHOPPING BAGS</p>
        <div className="all-bags">
          <div className="bags-left">
            <div className="bags-1">
              <img alt="" src="https://via.placeholder.com/65x65" />
              <p>Shopping Bags</p>
            </div>
          </div>
          <div className="bags-right" />
        </div>
      </div>
    </div>
    {/* <div className="choose-wrapper">
      <Choose />
    </div> */}
  </div>
);

export default Order;
