// of redux
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import userReducer from './containers/Users/reducer';

export default history =>
  combineReducers({
    router: connectRouter(history),
    user: userReducer,
  });
