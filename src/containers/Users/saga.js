import { call, put, takeLatest } from 'redux-saga/effects';
import { FETCH_USERS_REQUEST } from './constants';
import { fetchUsersSuccess, fetchUsersFailure } from './actions';
import { getUsers } from './api';

// gọi function fetchusers ->
// hàm getUsers gọi api -> trả về users,nếu success trả về function sc,nếu fail trả về err.
// export : function Saga của màn hình user
// init trạng thái đầu + thực hiện check khi load xong = func fetchUsers
export function* fetchUsers() {
  try {
    const users = yield call(getUsers);
    yield put(fetchUsersSuccess(users));
  } catch (err) {
    yield put(fetchUsersFailure(err));
  }
}

export default function* userSaga() {
  yield takeLatest(FETCH_USERS_REQUEST, fetchUsers);
}
