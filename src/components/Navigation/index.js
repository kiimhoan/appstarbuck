import React from 'react';
import { Link } from 'react-router-dom';
import './styles.scss';

const Navigation = () => (
  <div className="nav-wrapper">
    <div className="navigation-wrap">
      <Link to="/">
        <img className="logo" src="https://via.placeholder.com/48x48" alt="logo" />
      </Link>

      <div className="menu-items">
        <Link to="/order">ORDER</Link>
        <Link to="/card">CARDS</Link>
        <Link to="/users">USERS</Link>
        <Link to="/stores">STORES</Link>
      </div>
      <div className="menu-btn">
        <span>...</span>
      </div>
    </div>
  </div>
);

export default Navigation;
