import { FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS, FETCH_USERS_FAILURE } from './constants';

// khi chua co du lieu
export const initialState = {
  users: [],
  isFetchingUsers: false,
  isFetchingUsersError: false,
};

// reducer của màn hình user
/* eslint-disable default-case, no-param-reassign */
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_REQUEST:
      return {
        ...state,
        isFetchingUsers: true,
        isFetchingUsersError: false,
      };
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        isFetchingUsers: false,
        isFetchingUsersError: false,
        users: action.data,
      };
    case FETCH_USERS_FAILURE:
      return {
        ...state,
        isFetchingUsers: false,
        isFetchingUsersError: true,
      };
    default:
      return state;
  }
};

export default userReducer;
