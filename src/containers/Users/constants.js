// lưu trữ những actions constants trong trang local USERS
// export const REQUEST_ID = 'appstarbuck/Users/REQUEST_ID';
// export const GET_ID = 'appstarbuck/Users/GET_ID';
// export const GET_LOGIN = 'appstarbuck/Users/GET_LOGIN';
// export const GET_NODE_ID = 'appstarbuck/Users/GET_NODE_ID';

export const FETCH_USERS_REQUEST = 'users/FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'users/FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAILURE = 'users/FETCH_USERS_FAILURE';
