// Cach dat ten funciton API
// GET => getUser
// POST => createUser
// PUT => updateUser
// Map method do nhap nhang xu li
export const getUsers = async () => {
  try {
    const response = await fetch('https://api.github.com/users');
    const data = await response.json();
    return data;
  } catch (e) {
    throw e;
  }
};
