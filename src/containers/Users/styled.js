import styled from 'styled-components';

export const TitleWrapper = styled.div`
  font-size: 30px;
  text-align: center;
  margin-bottom: 30px;
  font-weight: bold;
  color: black;
`;
export const UsersWrapper = styled.div`
  padding: 0;
  .table-users {
    width: 100%;
    tr,
    th {
      text-align: center;
    }
  }
`;
