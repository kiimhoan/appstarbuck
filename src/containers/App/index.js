import React from 'react';
import { Route, Switch } from 'react-router'; // react-router v4/v5
import Home from '../Home';
import Order from '../Order';
import Users from '../Users';
import './styles.scss';

// () => = function ABC()
// dieu huong phan trang
function App() {
  return (
    <div className="app-wrapper">
      <Switch>
        <Route exact path="/" render={() => <Home />} />
        <Route exact path="/order" render={() => <Order />} />
        <Route exact path="/users" render={() => <Users />} />
        <Route render={() => <div>404 PAGE NOT FOUND</div>} />
      </Switch>
    </div>
  );
}

export default App;
